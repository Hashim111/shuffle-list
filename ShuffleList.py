from kivy.app import App
from kivy.uix.screenmanager import Screen
from kivy.uix.boxlayout import BoxLayout
from kivy.core.window import Window
from kivy.uix.widget import Widget
from kivy.uix.behaviors import DragBehavior
from kivy.uix.label import Label
from kivy.metrics import Metrics
class ShuffleItem(DragBehavior,Label):

    def __init__(self,**kwargs):
        print self.drag_rectangle
        super(ShuffleItem,self).__init__(**kwargs)
        # self.ids.label.text = kwargs['label_text']

        print self.drag_rectangle
        # print self.ids.label.text
        # print kwargs['label_text']

    def on_touch_down(self, touch):

        if self.collide_point(*touch.pos):
            self.pressed = touch.pos
            # we consumed the touch. return False here to propagate
            # the touch further to the children.
            print self.ids.label.text + " is touched"
        return super(ShuffleItem, self).on_touch_down(touch)


class ShuffleList(Screen):

    def __init__(self,**kwargs):
        super(ShuffleList,self).__init__(**kwargs)
        self.fake_generator()
        print self.ids

    def fake_generator(self):
        for i in range(5):
            si = ShuffleItem(id=str(i))
            si.ids.label.text = str(i)
            self.ids.shuffle_box.add_widget(si)

        pass
    # def on_touch_down(self, touch):

    def on_touch_up(self, touch):
        print self.ids.shuffle_box.children
        children =list(self.ids.shuffle_box.children)
        children.reverse()

        children.sort(key=lambda obj: obj.y, reverse=True)

        self.ids.shuffle_box.clear_widgets()
        for child in children:
            print child.id, child.pos
            si = ShuffleItem(id=child.id)
            si.ids.label.text = child.id
            self.ids.shuffle_box.add_widget(si)

        # return super(ShuffleList, self).on_touch_up(touch)


class ShuffleApp(App):
    def build(self):
        self.load_kv("ShuffleList.kv")
        print "Calling"
        return ShuffleList()

def on_motion(self, etype, motionevent):
    # will receive all motion events.
    # print motionevent
    pass

Window.bind(on_motion=on_motion)

if __name__=='__main__':
    ShuffleApp().run()


