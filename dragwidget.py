from kivy.uix.label import Label
from kivy.uix.boxlayout import BoxLayout
from kivy.app import App
from kivy.uix.behaviors import DragBehavior
from kivy.lang import Builder

# You could also put the following in your kv file...
kv = '''
<DragLabel>:
    # Define the properties for the DragLabel
    drag_rectangle: self.x, self.y, self.width, self.height
    drag_timeout: 10000000
    drag_distance: 0

<DragBoxLayout>:
    drag_rectangle: self.x, self.y, self.width, self.height
    drag_timeout: 10000000
    drag_distance: 0
    Label:
        text: 'Drag me'

        
FloatLayout:
    # Define the root widget
    # DragLabel:
    DragBoxLayout:
        size_hint: 0.25, 0.2
        # text: 'Drag me'
    DragBoxLayout:
        size_hint: 0.25, 0.2
    DragBoxLayout:
        size_hint: 0.25, 0.2    
    DragBoxLayout:
        size_hint: 0.25, 0.2
'''

class DragBoxLayout(DragBehavior,BoxLayout):
    pass

class DragLabel(DragBehavior, Label):
    pass


class TestApp(App):
    def build(self):
        return Builder.load_string(kv)

TestApp().run()